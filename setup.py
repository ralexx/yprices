# -*- coding: utf-8 -*-
from setuptools import setup, find_packages
with open('README.md') as f:
    readme = f.read()
with open('LICENSE') as f:
    license = f.read()
setup(
    name='yprices',
    version='0.2',
    description='The yprices package downloads historical stock prices from Yahoo! Finance.',
    long_description=readme,
    author='Rob Alexander',
    author_email='ralexx.mail@gmail.com',
    url='',
    license=license,
    packages=find_packages(exclude=('tests', 'docs'))
)
