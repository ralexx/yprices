The `yprices` package downloads historical stock prices from Yahoo! Finance. It experiences intermittent download failures (see "Known Issues").

Yahoo! Finance has changed their API and broken [Pandas Datareader](https://github.com/pydata/pandas-datareader). There is a thoughtful workaround package [available](https://github.com/ranaroussi/fix-yahoo-finance) but it doesn't include a `conda` recipe and various builds simply didn't work for me. This is my workaround to satisfy the historical stock price dependencies in my other projects.

## INSTALLATION
This package is built for Python 3.6 on OS X and is available on [Anaconda Cloud](https://anaconda.org/ralexx/).
```
conda install yprices -c ralexx
```

## USAGE
```python
pxhistory(ticker, start=None, end=None, session=None, **kwargs)
```
`ticker` is a single stock ticker in Yahoo's \<ticker>.\<exchange> format.

`start` and `end` are date strings formatted as `YYYY-MM-DD`.

### Additional keyword arguments
`session` is a `requests.Session`[ \[0\]](http://docs.python-requests.org/en/master/user/advanced/#session-objects) object to use for retrieving the prices (you can also use a `requests_cache.CachedSession`[ \[1\]](https://github.com/reclosedev/requests-cache)). This is needed to preserve cookies across requests and should be more efficient if calling this function multiple times for different tickers. If `session` is not passed, a `requests.Session` object will be instantiated when the fuction is called.

`adj_close` is a keyword argument to return both 'Close' and 'Adj Close' closing prices in the results DataFrame. By default, Yahoo! Finance's 'Close' field is suppressed and the
'Adj Close' field is renamed to 'Close'. The two fields are identical for most tickers; if you want the closing price on t-1 to match the opening price on t, the default setting achieves this. If you really want the unadjusted close included, pass `unadj_close=True`.

`retries` changes the number of times the function will attempt to download prices for a ticker. This is primarily intended to mitigate an intermittent failure to identify the cookie "crumb" needed to complete the download GET request. Default is `retries=3`.

`interval` sets the delay time in seconds between retries.

### Examples
```python
In [1]: from yprices import pxhistory

In [2]: pxhistory('C', start='2017-05-31', end='2017-05-31')
Out[2]:
  Ticker        Date   Open       High        Low      Close    Volume
0      C  2017-05-31  61.59  61.599998  59.869999  60.540001  23150500

In [3]: pxhistory('C', start='2017-05-31', end='2017-05-31').set_index(['Ticker', 'Date'])
Out[3]:
                    Open       High        Low      Close    Volume
Ticker Date
C      2017-05-31  61.59  61.599998  59.869999  60.540001  23150500

In [4]: pxhistory('C',
                    start='2017-05-31',
                    end='2017-05-31',
                    unadj_close=True).set_index(['Ticker', 'Date'])
Out[4]:
                    Open       High        Low      Close  Adj Close    Volume
Ticker Date
C      2017-05-31  61.59  61.599998  59.869999  60.540001  60.540001  23150500
```
## KNOWN ISSUES
Periodically, the Yahoo server does not return a cookie "crumb" as expected. This may be due to rate limiting; I'm unsure of the cause. You can trade off between download failures and slow performance by adjusting the `retries` argument of the `pxhistory` function.

In my experience the best workaround is to plan for download failures and wrap the `pxhistory` function in error-handling code.

## CHANGES

### Version 0.2 : 2017/07/03
* Add `incremental_history` function to attempt downloading historical data by month in reverse chronological order. This (tries to) fix the crumb error when the requested date range exceeds Yahoo's available history
* Refactor `pxhistory` to allow fast and slow download paths
* Use the `arrow` package to quickly calculate date ranges

### Version 0.1 : 2017/06/28
* Default values for start date (today-2) and end date (today-1)
* Added delay interval to download retries

### Version 0.0 : 2017/06/27
* Download prices and return as DataFrame.
* Retry on failed requests and warn if retries are unsuccessful.
* Multiple attempts to scrape cookie "crumb"
* Warning message for empty download (e.g. due to date mismatch with Asia time)
