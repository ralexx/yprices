# -*- coding: utf-8 -*-
from collections import OrderedDict
from datetime import datetime, timedelta
from io import StringIO
# import os
import re
from time import sleep
from warnings import warn

import arrow
import pandas as pd
import requests

__all__ = ['pxhistory', 'incremental_history']

HIST_URL = 'https://finance.yahoo.com/quote/{ticker}/history?p={ticker}'

def dateseconds(datestring):
    """Express the datestring YYYY-MM-DD as the timedelta of datestring minus
    January 1, 1970, in seconds."""
    match_string = r'(?P<year>\d{4})-(?P<mon>\d\d)-(?P<day>\d\d)'
    date_parse = re.match(match_string, datestring)
    if not date_parse:
        raise ValueError('Date string must be passed as YYYY-MM-DD.')

    # Yahoo's query strings format the dates as offsets from 1970-01-01 09:00:00
    basedate = datetime(1970, 1, 1, 9)
    date_parts = [int(date_parse.group(n)) for n in range(1, 4)]
    # construct the lookup date as of US close i.e. 16:00:00
    lookup_date = datetime(*date_parts, 16)
    return str(int((lookup_date - basedate).total_seconds()))

def cookie_crumbs(ticker, session, retries=3, on_err='warn', flush=True, **kwargs):
    """Request historical data landing page and extract cookie 'crumb' from the embedded
    JSON. Crumb is needed for the historical data download GET request string.

    Parameters
    ========
    ticker: str
    ression : requests.Session or requests-cache.CachedSession
    on_err : {'warn', 'raise}
        Determines the flow control of an AttributeError from the regex search
        not finding a crumb.
    flush : bool
        Whether to flush cookies from the Session/CachedSession before attempting
        the next GET request for a new cookie crumb.
    """
    hist_url = HIST_URL.format(ticker=ticker)
    retries = max(retries, 1)
    original_retries = retries
    msg = 'Could not identify the crumb for ticker "{}". {}'

    while retries > 0:
        hist_page_req = session.get(hist_url)
        crumb_search = re.search(r'"CrumbStore"\:\{"crumb":"(\w+?)"\}', hist_page_req.text)
        if not crumb_search:
            crumb_search = re.search(r'{"crumb":"(\w+?)"', hist_page_req.text)

        if crumb_search:
            # we got what we needed; return it
            break
        else:
            if flush:
                try:
                    session.cookies.clear()
                except KeyError:
                    pass
            warn(msg.format(ticker, 'Retrying...'))
            sleep(max(kwargs.get('interval', 5), 5))
            retries -= 1
    else:
        # check for argument to force an error rather than a warning
        if on_err == 'raise':
            raise ValueError(msg.format(ticker, ''))
        else:
            warn(msg.format(ticker, ''))
            return 0

    return crumb_search.group(1)

def dl_engine(ticker, start, end, requestor, crumb=None, **kwargs):
    """Download historical data from Yahoo Finance. Requires a 'crumb' obtained from
    the `cookie_crumbs` function."""
    query_url = ('https://query1.finance.yahoo.com/v7/finance/download/{ticker}'
                 .format(ticker=ticker))
    # default values for start and end dates
    if datetime.today().hour < 16:
        default_end = (datetime.today() + timedelta(days=-1))
    else:
        default_end = datetime.today()
    default_start = (default_end + timedelta(days=-1))
    startdate = dateseconds(start or default_start.strftime('%Y-%m-%d'))
    enddate = dateseconds(end or default_end.strftime('%Y-%m-%d'))

    try:
        crumb = crumb or cookie_crumbs(ticker, requestor, **kwargs)
        payload = OrderedDict([
            ('period1', startdate),
            ('period2', enddate),
            ('interval', '1d'),
            ('events', 'history'),
            ('crumb', crumb)])
    except UnboundLocalError:
        raise ValueError('Failed to get crumb for {}.'.format(ticker))
    # data download
    histdata = requestor.get(query_url, params=payload)
    return histdata

def incremental_history(ticker, start, end, requestor, **kwargs):
    """Attempt to download a ticker's history in reverse chronological order,
    one month per request. Return the requests that didn't fail."""
    container = []
    start_obj, end_obj = arrow.get(start), arrow.get(end)
    date_range = arrow.Arrow.span_range('month', start_obj, end_obj)
    date_range.reverse()
    # attempt to download one-month blocks
    for startdt, enddt in date_range:
        try:
            histdata = dl_engine(ticker,
                                    startdt.format('YYYY-MM-DD'),
                                    enddt.format('YYYY-MM-DD'),
                                    requestor,
                                    **kwargs)
        except ValueError as err1:
            print(err1)
        else:
            frame = pd.read_csv(StringIO(histdata.text))
            container.append(frame)
    return pd.concat(container).sort_values('Date')

def pxhistory(ticker, start, end, session=None, **kwargs):
    """Scrape prices from Yahoo Finance. Use session to pass a requests.Session or
    reqeusts_cache.CachedSession object in.

    Parameters
    ========
    ticker : str
    start, end : str of format 'YYYY-MM-DD'
        Defaults to start=today()-2, end=today()-1
    session : requests.Session or requests_cache.CachedSession
    ad_close : bool
        Pass adj_close=False to keep both Close and Adj Close price columns in the
        output (removes Close by default).
    retries : int > 0
        How many retries in the event that the GET request fails to return a cookie crumb.
    """
    head_pkg = {'User_Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 \
                (KHTML, like Gecko) Chrome/53.0.2785.34 Safari/537.36'}

    # request object selection
    if session:
        requestor = session
    else:
        requestor = requests.Session()

    requestor.headers.update(head_pkg)

    # ask for cookie "crumb"; fast path with success, slow path with failure
    crumb = cookie_crumbs(ticker, requestor)
    empty_msg = ('{} returned no data for those dates. Try changing start and/or end parameters.'
                .format(ticker))

    # fast path: download full date range in a single request
    if crumb:
        for i in range(max(1, kwargs.get('retries', 3))):
            # histdata = requestor.get(query_url, params=payload, headers=head_pkg)
            histdata = dl_engine(ticker, start, end, requestor)
            # print(histdata.text)  # TEST
            if 'Encountered an error when generating the download data' in histdata.text:
                warn(empty_msg + 'pass {}'.format(i))
            # convert to DataFrame and insert ticker column
            frame = pd.read_csv(StringIO(histdata.text))
            if len(frame.values) > 0:
                break

    # slow path: iterate through one-month date ranges in reverse chronological order
    else:
        print('*CRUMB NOT FOUND* Taking slow path')  # TEST
        frame = incremental_history(ticker, start, end, requestor, on_err='raise')

    # test for empty DataFrame
    if len(frame.values) == 0:
        warn(empty_msg)
        return 0

    # assign ticker to each row
    frame.insert(0, 'Ticker', ticker)

    # drop unadjusted close unless overridden
    if kwargs.get('unadj_close'):
        try:
            frame = frame.drop('Close', axis=1)
        except ValueError as err2:
            if "labels ['Close'] not contained in axis" in str(err2):
                pass
        else:
            frame = frame.rename(columns={'Adj Close': 'Close'})
    return frame
