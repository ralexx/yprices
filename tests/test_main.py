# -*- coding: utf-8 -*-
from io import StringIO
from random import choice
import re

from pandas import DataFrame, read_csv
from requests import Session
from yprices import main
import yprices

def test_dateseconds():
    """Function to convert date strings into seconds since 1970/01/01."""
    test_date = '2017-05-31'
    expected = '1496214000'

    result1 = main.dateseconds(test_date)
    # print(result1)
    assert str(result1) == expected

def test_crumbs_true():
    """Parses cookie 'crumb' from JSON source."""
    test_ticker = 'C'
    sesh = Session()
    result = main.cookie_crumbs(test_ticker, sesh, interval=1)
    assert result != 0
    assert re.match(r'\w{11}', result)

def test_dl_engine():
    """The core download function."""
    test_ticker = 'C'
    sesh = Session()
    test_date = '2017-05-31'
    expected = [['C', '2017-05-31', 61.59, 61.599998, 59.869999,
                60.540001000000004, 60.540001000000004, 23150500]]

    histdata = main.dl_engine(test_ticker, test_date, test_date, sesh)
    # print(histdata.text)
    frame = read_csv(StringIO(histdata.text))
    frame.insert(0, 'Ticker', test_ticker)
    # print(frame)
    assert frame.values.tolist() == expected

def test_incremental():
    """Loop through download attempts month by month in reverse chronological order."""
    test_ticker = 'C'
    sesh = Session()
    start_date = '2016-12-31'
    end_date = '2017-05-31'

    result = yprices.incremental_history(test_ticker, start_date, end_date, sesh, on_err='raise')
    assert isinstance(result, DataFrame)
    assert result.shape[0] != 0
    assert (result['Date'].min(), result['Date'].max()) == ('2016-12-01', '2017-05-31')
    # print(result.head())
    # assert 0

def test_pxhistory():
    """Scrape the historical price download and return as Pandas DataFrame."""
    tickers1 = ['C', 'DPW.DE']
    tickers2 = ['0002.HK', '601988.SS', '000001.SZ']
    test_start1 = '2017-05-31'
    test_start2 = '2017-05-30'
    test_end = test_start1
    expected = {
        'DPW.DE': [['DPW.DE', '2017-05-31', 31.809998999999998, 32.715,
                    31.799999, 32.514998999999996, 7199669]],
        'C': [['C', '2017-05-31', 61.59, 61.599998, 59.869999,
               60.540001000000004, 23150500]],
        '601988.SS': [['601988.SS', '2017-05-31', 3.7, 3.74, 3.69, 3.73, 237648223]],
        '0002.HK': [['0002.HK', '2017-05-31', 84.099998, 85.25, 83.449997,
                     84.610687, 5779480]],
        '000001.SZ': [['2017-05-31', 9.10, 9.23, 9.06, 9.20, 9.20, 103321095]]
        }
    sesh = Session()

    container = {}

    # test using a requests.Session
    while len(tickers1) > 0:
        candidate = tickers1.pop(tickers1.index(choice(tickers1)))
        container[candidate] = (main.pxhistory(candidate, test_start1, test_end, session=sesh).values.tolist())
    while len(tickers2) > 0:
        candidate = tickers2.pop(tickers2.index(choice(tickers2)))
        container[candidate] = (main.pxhistory(candidate, test_start2, test_end, session=sesh).values.tolist())

    # print(container)
    for each in tickers1 + tickers2:
        assert container[each] == expected[each]

def test_date_error(capsys):
    """Handle the error when requesting a single date that does not match up to Asia time."""
    test_start1 = '2017-05-31'
    test_start2 = '2017-05-30'
    test_end = test_start1

    result = yprices.pxhistory('601988.SS', start=test_start1, end=test_end)
    if isinstance(result, DataFrame):
        print(result.head())
    else:
        assert result == 0

# def test_pxhistory_defaults():
#     """Verify default argument values work."""
#     tickers1 = ['C', 'DPW.DE']
#     tickers2 = ['0002.HK', '601988.SS']
#     selection = choice(tickers1 + tickers2)

#     result = yprices.pxhistory(selection)
#     assert isinstance(result, DataFrame)
